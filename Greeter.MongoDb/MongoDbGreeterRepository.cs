using Greeter.Core;
using MongoDB.Driver;
namespace Greeter.MongoDb;

internal class PersistentGreeter {
    public string Id { get; set; }
    public string BaseMessage { get; set; }

    public Core.Greeter ToModel() {
        return new Core.Greeter(BaseMessage);
    }
}

public class MongoDbGreeterRepository : GreeterRepository {
    public const string CollectionName = "greeters";
    
    private readonly IMongoCollection<PersistentGreeter> collection;
    public MongoDbGreeterRepository(MongoDatabaseGateway db) {
        collection = db.GetCollection<PersistentGreeter>(CollectionName);
    }

    public async Task<IReadOnlyList<Core.Greeter>> GetAll() {
        var cursor = await collection.FindAsync(Builders<PersistentGreeter>.Filter.Empty);
        var docs = await cursor.ToListAsync();
        return docs.Select(x => x.ToModel()).ToList();
    }

    public async Task<Core.Greeter> GetById(string greeterId) {
        var cursor = await collection.FindAsync(Builders<PersistentGreeter>.Filter.Eq(x => x.Id, greeterId));
        var doc = await cursor.SingleOrDefaultAsync();
        return doc?.ToModel();
    }
}
