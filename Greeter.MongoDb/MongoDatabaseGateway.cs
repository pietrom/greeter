using MongoDB.Driver;

namespace Greeter.MongoDb;

public class MongoDbConfiguration {
    public string ConnectionString { get; init; }
    public string Database { get; init; }
}

public class MongoDatabaseGateway {
    private readonly MongoDbConfiguration configuration;
    private readonly IMongoDatabase db;

    public MongoDatabaseGateway(MongoDbConfiguration configuration) {
        this.configuration = configuration;
        var client = new MongoClient(configuration.ConnectionString);
        db = client.GetDatabase(configuration.Database);
    }

    public IMongoCollection<T> GetCollection<T>(string collectionName) {
        return db.GetCollection<T>(collectionName);
    }
}
