using System;

namespace Greeter.IntegrationTest;

public static class MongoDbTestConnectionHelper {
    public static string ConnectionString => Environment.GetEnvironmentVariable("INTEGRATION_TEST_CONNECTION_STRING") ??
                                             "mongodb://localhost:27019/";
}
