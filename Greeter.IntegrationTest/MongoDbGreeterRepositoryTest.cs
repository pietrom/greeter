using System.Linq;
using System.Threading.Tasks;
using Greeter.Core;
using Greeter.MongoDb;
using MongoDB.Bson;
using NUnit.Framework;

namespace Greeter.IntegrationTest;

public class MongoDbGreeterRepositoryTest {
    private static readonly MongoDbConfiguration MongoDbConfig = new MongoDbConfiguration {
        ConnectionString = MongoDbTestConnectionHelper.ConnectionString,
        Database = "greetertest"
    };

    private GreeterRepository target;
    private MongoDbHelper helper;

    [SetUp]
    public void Setup() {
        var databaseGateway = new MongoDatabaseGateway(MongoDbConfig);
        target = new MongoDbGreeterRepository(databaseGateway);
        helper = new MongoDbHelper(MongoDbConfig);
    }

    [Test]
    public async Task ShouldGetAllAvailableGreeters() {
        await helper.Clear(MongoDbGreeterRepository.CollectionName);
        await helper.Insert(MongoDbGreeterRepository.CollectionName, new BsonDocument {
            { "_id", "x" },
            { "BaseMessage", "Hello"}
        });
        await helper.Insert(MongoDbGreeterRepository.CollectionName, new BsonDocument {
            { "_id", "y" },
            { "BaseMessage", "Ciao"}
        });
        await helper.Insert(MongoDbGreeterRepository.CollectionName, new BsonDocument {
            { "_id", "z" },
            { "BaseMessage", "Hola"}
        });

        var fromDatabase = await target.GetAll();
        
        Assert.That(fromDatabase.ToArray(), Is.EquivalentTo(new[] {
            new Core.Greeter("Hello"),
            new Core.Greeter("Ciao"),
            new Core.Greeter("Hola")
        }));
    }
    
    [Test]
    public async Task ShouldGetGreeterById() {
        await helper.Clear(MongoDbGreeterRepository.CollectionName);
        await helper.Insert(MongoDbGreeterRepository.CollectionName, new BsonDocument {
            { "_id", "x" },
            { "BaseMessage", "Hello"}
        });

        var fromDatabase = await target.GetById("x");
        
        Assert.That(fromDatabase, Is.EqualTo(new Core.Greeter("Hello")));
    }
    
    [Test]
    public async Task CantGetGreeterByUnknownId() {
        await helper.Clear(MongoDbGreeterRepository.CollectionName);       
        await helper.Insert(MongoDbGreeterRepository.CollectionName, new BsonDocument {
            { "_id", "x" },
            { "BaseMessage", "Hello"}
        });

        var fromDatabase = await target.GetById("y");
        
        Assert.That(fromDatabase, Is.Null);
    }
}