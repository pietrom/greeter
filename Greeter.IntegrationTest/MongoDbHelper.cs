using System.Threading.Tasks;
using Greeter.MongoDb;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Greeter.IntegrationTest;

internal class MongoDbHelper {
    private IMongoDatabase db;

    public MongoDbHelper(MongoDbConfiguration config) {
        var client = new MongoClient(config.ConnectionString);
        db = client.GetDatabase(config.Database);
    }

    private IMongoCollection<BsonDocument> GetCollection(string collectionName) {
        return db.GetCollection<BsonDocument>(collectionName);
    }

    public Task Insert(string collectionName, BsonDocument doc) {
        return GetCollection(collectionName).InsertOneAsync(doc);
    }

    public Task Clear(string collectionName) {
        return GetCollection(collectionName).DeleteManyAsync(Builders<BsonDocument>.Filter.Empty);
    }
}
