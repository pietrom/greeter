using NUnit.Framework;

namespace Greeter.Core.Test; 

[TestFixture]
public class GreeterTest {
    [Test]
    public void ShouldBuildMessage() {
        var greeter = new Greeter("Hello");
        var message = greeter.BuildMessage("World");
        Assert.That(message, Is.EqualTo("Hello, World!"));
    }
}
