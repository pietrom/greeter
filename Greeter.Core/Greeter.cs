namespace Greeter.Core; 

public record Greeter(string BaseMessage) {
    public string BuildMessage(string to) {
        return $"{BaseMessage}, {to}!";
    }
}
