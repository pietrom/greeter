using System.Collections.Generic;
using System.Threading.Tasks;

namespace Greeter.Core; 

public interface GreeterRepository {
     Task<IReadOnlyList<Greeter>> GetAll();
     
     Task<Greeter> GetById(string greeterId);
}
